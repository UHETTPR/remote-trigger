#Import Libraries
import requests
import os
import json
import time
import re
import sys 

# seconds for polling Jenkins API
QUEUE_POLL_INTERVAL = 5
JOB_POLL_INTERVAL = 10
OVERALL_TIMEOUT = 120 # 2 minutes

try:
    #Get current folder path
    full_path = os.path.realpath(__file__)
    cwd = os.path.dirname(full_path)
    print (f"[Info]: Current directory is: {cwd}")

    #Read the config file
    configFile=open(cwd+"/config.json","r")
    configContent = configFile.read()
    jsonConfigContent = json.loads(configContent)

    #Parse the config file
    jenkins_url=jsonConfigContent['jenkins_url']
    user=jsonConfigContent['user']
    user_token=jsonConfigContent['user_token']
    job_name=jsonConfigContent['job_name']
    job_token=jsonConfigContent['job_token']

except Exception as e:
    print(f"[Error]:In using the configuration:{e}")
    raise SystemExit(1) 

#Send the post request and check the status
url=jenkins_url+"/job/"+job_name+"/build?token="+job_token
result = requests.post(url,auth=(user,user_token))

print("Initiating...")
if(result.status_code != 201):
    # from return headers get job queue location
    m = re.match(r"http.+(queue.+)\/", result.headers['Location'])
    if not m:
        print (f"[Warning]:Job starter request did not have queue location")
        raise SystemExit(1) 
    else:
        print("Job Initiation Failed with Status Code "+str(result.status_code))
        raise SystemExit(1) 
else:
    print("Job Initiated with Status Code "+str(result.status_code))

elasped_time = 0 
AfterBuildurl  = jenkins_url+"/job/"+job_name+"/lastBuild/api/json"
data = requests.get(AfterBuildurl,auth=(user,user_token)).json()

# poll the queue looking for job to start
while(data['building']==False):
    print("pending...")
    data = requests.get(AfterBuildurl,auth=(user,user_token)).json()
    if(data['building']==True):
        print("Job is running")
        break
    time.sleep(QUEUE_POLL_INTERVAL)
    elasped_time += QUEUE_POLL_INTERVAL
    if (elasped_time % (QUEUE_POLL_INTERVAL * 10)) == 0:
        print(f"[Warning]: No service nodes available")
        raise SystemExit(1)
  
# poll job status waiting for a result
start_epoch = int(time.time())
while(data['building']==True):
    time.sleep(JOB_POLL_INTERVAL)
    data = requests.get(AfterBuildurl,auth=(user,user_token)).json() 
    if (data['result'] == 'SUCCESS'):
        # Do success steps
        print ("[PASSED]")
        break
    if (data['result'] == 'FAILURE'):
        # Do failure steps
        print ("[FAILED]")
        raise SystemExit(1)
    cur_epoch = int(time.time())
    if (cur_epoch - start_epoch) > OVERALL_TIMEOUT:
        print ("No status before timeout")
        raise SystemExit(1)
    data = requests.get(AfterBuildurl,auth=(user,user_token)).json()    


	                             